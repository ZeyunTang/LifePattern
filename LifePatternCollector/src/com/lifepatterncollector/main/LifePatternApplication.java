package com.lifepatterncollector.main;

import java.util.LinkedList;
import java.util.List;

import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;

import com.baidu.mapapi.SDKInitializer;
import com.lifepattern.baidulocation.LocationService;
import com.lifepatterncollector.addtag.TagInterval;
import com.lifepatterncollector.sensor.WiFiEntity;

public class LifePatternApplication extends Application{	
	//通知间隔时间
	public int notificationIntervalMinute = 60;
	//传感器数据采集间隔时间
	public int sensorIntervalSeconds = 10;
	//标记间隔时间
	public int tagIntervalMinutes = 15;
	
	//默认闹钟时间
	public int alarmHour = 7;
	public int alarmMinute = 30;
	
	//尚未标记的时间段
	public List<TagInterval> unLabledInterval = new LinkedList<TagInterval>();
	//当前记录时间开始时间
	public int beginMonth;
	public int beginDay;
	public int beginHour;
	public int beginMinutes;
	//当前记录结束时间
	public int endMonth;
	public int endDay;
	public int endHour;
	public int endMinutes;	
	
	//wifi列表
	public WiFiEntity[] wifiRes;
	//app列表
	public String[] appRes;	
    
	public LocationService locationService;
	
    @Override
    public void onCreate() {
        super.onCreate();
        /***
         * 初始化定位sdk，建议在Application中创建
         */
        
      	locationService = new LocationService(getApplicationContext());
        SDKInitializer.initialize(getApplicationContext());  
        
        IntentFilter filter = new IntentFilter(Intent.ACTION_TIME_TICK);        
        BootstrapReceiver receiver = new BootstrapReceiver();   
        registerReceiver(receiver, filter);   
       
    }
}
