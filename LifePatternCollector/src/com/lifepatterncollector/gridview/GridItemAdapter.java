package com.lifepatterncollector.gridview;

import com.lifepatterncollector.main.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridItemAdapter extends BaseAdapter {
	private int focusPosition = -1;
	
	private LayoutInflater inflater;   
	private int[] images;
	private String[] describtion; 
	
	public GridItemAdapter(int[] images,String[] describtion,Context context){
		this.images = images;
		this.describtion = describtion;
		this.inflater = LayoutInflater.from(context);		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return describtion.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return describtion[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;   
        if (convertView == null)   
        {   
            convertView = inflater.inflate(R.layout.griditem, null);   
            viewHolder = new ViewHolder();     
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);  
            viewHolder.describtion = (TextView) convertView.findViewById(R.id.description);   
            convertView.setTag(viewHolder);   
        } else  
        {   
            viewHolder = (ViewHolder) convertView.getTag();   
        }   
        viewHolder.describtion.setText(describtion[position]);     
        viewHolder.image.setImageResource(images[position]);
        if(position == focusPosition){
        	viewHolder.image.setBackgroundResource(R.color.yellow);
        }else{
        	viewHolder.image.setBackgroundResource(0);
        }
        
        return convertView; 
	}
	
	public void setFocusPosition(int position){
		this.focusPosition = position;
	}
	
	public int getFocusPosition(){
		return this.focusPosition;
	}
	
	private  class ViewHolder   
	{   
	    public ImageView image;   
	    public TextView describtion;    
	}   
}
