package com.lifepatterncollector.sensor;

import java.io.Serializable;

public class WiFiEntity implements Serializable{
	private String macAddress;
	private int level;
	
	public WiFiEntity(String macAddress, int level) {
		super();
		this.macAddress = macAddress;
		this.level = level;
	}
	
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
}
