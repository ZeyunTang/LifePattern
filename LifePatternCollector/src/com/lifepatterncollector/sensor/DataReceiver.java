package com.lifepatterncollector.sensor;

import com.lifepatterncollector.main.MainActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


public class DataReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		 Bundle bundle=intent.getExtras();
         MainActivity.mainActivity.gps = bundle.getDoubleArray("gps");
         MainActivity.mainActivity.acc = bundle.getDoubleArray("acc");
         MainActivity.mainActivity.phoneMode = bundle.getInt("mode");
         MainActivity.mainActivity.isCharging = bundle.getInt("ischarging");
         
         MainActivity.mainActivity.updateLocation();
         MainActivity.mainActivity.updateAccelerator();
	}
}