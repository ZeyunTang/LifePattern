package com.lifepatterncollector.addtag;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.widget.HorizontalScrollView;
import android.widget.Toast;

public class MyScrollView extends HorizontalScrollView {
	private GestureDetector mGestureDetector;
	
	private Context context;
	private int subChildCount = 0;
	private ViewGroup firstChild = null;
	private int downX = 0,exitX = 0;
	private boolean downXed = false;
	
	private int currentPage = 0;
	private ArrayList<Integer> pointList = new ArrayList<Integer>();
	
	public MyScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		init();
	}


	public MyScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}

	public MyScrollView(Context context) {
		super(context);
		this.context = context;
		init();
	}
	private void init() {
//		mGestureDetector = new GestureDetector(new HScrollDetector());
		setHorizontalScrollBarEnabled(false);
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		receiveChildInfo();
	}
	public void receiveChildInfo() {
		
		firstChild = (ViewGroup) getChildAt(0);
		if(firstChild != null){
			subChildCount = firstChild.getChildCount();
			for(int i = 0;i < subChildCount;i++){
				if(((View)firstChild.getChildAt(i)).getWidth() > 0){
					pointList.add(((View)firstChild.getChildAt(i)).getLeft());
				}
			}
		}

	}
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
			switch (ev.getAction()) {
				case MotionEvent.ACTION_DOWN:
					downX = (int) ev.getX();
					downXed = true;
					break;
				case MotionEvent.ACTION_MOVE:{
					if(!downXed){
						downX = (int)ev.getX();
						downXed = true;
					}
				}break;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:{
					if( Math.abs((ev.getX() - downX)) > getWidth() / 20){
						if(ev.getX() - downX > 0){
//							smoothScrollToPrePage();
						}else{
//							smoothScrollToNextPage();
						}
					}
					downXed = false;
					return true;
				}
			}
		return true;
	}

	private void smoothScrollToCurrent() {
		smoothScrollTo(pointList.get(currentPage), 0);
	}

	private void smoothScrollToNextPage() {
		if(currentPage < subChildCount - 1){
			currentPage++;
			smoothScrollTo(pointList.get(currentPage), 0);
		}
		
		if(currentPage == subChildCount - 1){
			Toast.makeText(context,"已到最后一页",Toast.LENGTH_SHORT).show();
		}
	}

	private void smoothScrollToPrePage() {
		if(currentPage > 0){			
			currentPage--;
			smoothScrollTo(pointList.get(currentPage), 0);
		}
		
		if(currentPage == 0){
			Toast.makeText(context,"已到第一页",Toast.LENGTH_SHORT).show();
		}
	}
	/**
	 * 下一页
	 */
	public void nextPage(){
		smoothScrollToNextPage();
	}
	/**
	 * 上一页
	 */
	public void prePage(){
		smoothScrollToPrePage();
	}
	/**
	 * 跳转到指定的页面
	 * @param page
	 * @return
	 */
	public boolean gotoPage(int page){
		if(page > 0 && page < subChildCount - 1){
			smoothScrollTo(pointList.get(page), 0);
			currentPage = page;
			return true;
		}
		return false;
	}
}
