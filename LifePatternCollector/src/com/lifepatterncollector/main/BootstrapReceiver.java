package com.lifepatterncollector.main;


import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.lifepatterncollector.sensor.SensorService;

public class BootstrapReceiver extends BroadcastReceiver {

	@Override
    public void onReceive(Context context, Intent intent) {
		if(!isServiceRunning(context)){
			context.startService(new Intent(context, SensorService.class));
		}    	
    }
	
	private boolean isServiceRunning(Context context) {
	    ActivityManager manager = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if ("com.lifepatterncollector.sensor.SensorService".equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
}
