package com.lifepatterncollector.sensor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.lifepatterncollector.addtag.AddTagActivity;
import com.lifepatterncollector.main.MainActivity;
import com.lifepatterncollector.main.R;

public class SensorService extends Service{	 
	public static SensorService sensorService;
	
	private String tag = "SensorService";
	private static int handlerBroadCast = 1;
	
//	private static LifePatternApplication app;
	
    //Acc
    private SensorManager sensorManager;
    private double[] acc = new double[3];
    private SensorServiceListener sensorServiceListener;
    
    private File dataFile;
	private FileWriter dataFileWriter; 
	
	private File tagFile;
	private FileWriter tagFileWriter;
    
    //GPS
    private LocationManager locationManager;
    private LocationListener locationListener;    
    private static final long minTime = 2000;
    private static final float minDistance = 10;
    private double[] gps = new double[3];
//    private LocationService locationService;
//	private LinkedList<LocationEntity> locationList = new LinkedList<LocationEntity>(); // 存放历史定位结果的链表，最大存放当前结果的前5次定位结果
 
    //WiFi
    private static WifiManager wifiManager;
    private static WiFiEntity[] wifiRes = new WiFiEntity[16];
    
    //Phone Mode
    private AudioManager audioManager;
    private int phoneMode;
    
    //Charging status
    private IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
    private int isCharging;
    
    //App Usage
    private ActivityManager activityManager;
    private String[] appRes = new String[5];
    
    private final IBinder mBinder = new GPSServiceBinder(); 
    
    private boolean run = true;
    
    private Handler handler = new Handler() {  
        public void handleMessage(Message msg) {  
            if (msg.what == handlerBroadCast) {
//            	Intent intent=new Intent();
//                intent.putExtra("gps", gps);
//                intent.putExtra("acc", acc);
//                intent.putExtra("mode", phoneMode);
//                intent.putExtra("ischarging", isCharging);
//                
//                intent.setAction("SensorService");
//                sendBroadcast(intent);
                
                writeData();
            }  
        };  
    };
    
    public void startService()
    {
    	
    	//创建App目录
		File appDir = new File(Environment.getExternalStorageDirectory().getPath() + "/LifePattern"); 
		if(!appDir.exists()){
			appDir.mkdir();
		}
		
		createFile();
    	
    	//Acc
    	sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    	sensorServiceListener = new SensorServiceListener();
    	sensorManager.registerListener(sensorServiceListener,sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL);
    	//GPS
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);        
        locationListener = new GPSServiceListener();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, locationListener);    	
//		locationService = ((LifePatternApplication)getApplication()).locationService;
//    	
//    	LocationClientOption mOption = locationService.getDefaultLocationClientOption();
//		locationService.setLocationOption(mOption);
//		locationService.registerListener(listener);
//		locationService.start();
    	
    	
    	//WiFi
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        openWifi();
        
        //PhoneMode
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        
        //App usage
        activityManager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE) ;
        
        new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while (run) {  
		            try {  
		            	scanWifi();
		            	
		            	phoneMode = audioManager.getRingerMode();
		            	
		            	Intent batteryStatus = registerReceiver(null, ifilter);
		            	int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
		            	if(status == BatteryManager.BATTERY_STATUS_CHARGING){
		            		isCharging = 1;
		            	}else{
		            		isCharging = 0;
		            	}
		            	
		            	List<RunningTaskInfo> appList = activityManager.getRunningTasks(100);
		            	int i;
		            	for(i = 0; i < Math.min(5, appList.size()); i++){
		            		appRes[i] = appList.get(i).topActivity.getPackageName();
		            	}
		            	
		            	for(; i < 5; i++){
		            		appRes[i] = "NAN";
		            	}
		            	
		            	boolean isAppRunning = false; 
		            	String MY_PKG_NAME = "com.lifepatterncollector.main"; 
		            	for (RunningTaskInfo info : appList) { 
			            	if (info.topActivity.getPackageName().equals(MY_PKG_NAME) || info.baseActivity.getPackageName().equals(MY_PKG_NAME)) { 
			            		isAppRunning = true; 		            	
			            	break; 
			            	} 
		            	}
		            	
		            	if(!isAppRunning){
		            		Intent intent = new Intent(SensorService.this,MainActivity.class);
		                	startActivity(intent); 
		            	}
		            	
		                Message msg = new Message();  
		                msg.what = handlerBroadCast;  
		                handler.sendMessage(msg);  
		                Thread.sleep(10000);  
		            } catch (Exception e) {  
		                // TODO Auto-generated catch block  
		                e.printStackTrace();  
		            }  
		        }  
			}
		}).start();
    }
 
    public void endService()
    {
        if(locationManager != null && locationListener != null)
        {
            locationManager.removeUpdates(locationListener);
        }

//    	locationService.unregisterListener(listener);
//		locationService.stop();
        
        sensorManager.unregisterListener(sensorServiceListener);
        
        run = false;
        
        closeFile();
    }
    
    /***
	 * 定位结果回调，在此方法中处理定位结果
	 */
	BDLocationListener listener = new BDLocationListener() {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// TODO Auto-generated method stub

			if (location != null && location.getLocType() == 61) {
//				Algorithm(location);
				
				gps[0] = location.getLongitude();
				gps[1] = location.getLatitude();
				gps[2] = location.getSpeed();
			}
		}
	};
	
	public void createFile(){				
		dataFile = new File(Environment.getExternalStorageDirectory().getPath() + "/LifePattern","data.txt");
		tagFile = new File(Environment.getExternalStorageDirectory().getPath() + "/LifePattern","tag.txt");
		
		boolean dataFileAbsent = false;
		boolean tagFileAbsent = false;
		
		try {
			if(!dataFile.exists()){
				dataFile.createNewFile();
				dataFileAbsent = true;
			}
			
			dataFileWriter = new FileWriter(dataFile,true);
			if(dataFileAbsent){
				dataFileWriter.write("month day hour minutes seconds week " +
						"accX accY accZ GPSLon GPSLat GPSSpeed phoneState " +
						"isCharging connectedWiFi connectedLevel WiFi1 level1 " +
						"WiFi2 level2 WiFi3 level3 WiFi4 level4 WiFi5 level5 WiFi6 level6 " +
						"WiFi7 level7 WiFi8 level8 WiFi9 level9 WiFi10 level10 WiFi11 level11 " +
						"WiFi12 level12 WiFi13 level13 WiFi14 level14 WiFi15 level15 " +
						"app1 app2 app3 app4 app5\n");
				dataFileWriter.flush();
			}
			
			if(!tagFile.exists()){
				tagFile.createNewFile();
				tagFileAbsent = true;
			}
			
			tagFileWriter = new FileWriter(tagFile,true); 
			if(tagFileAbsent){
				tagFileWriter.write("month day hour minute tag\n");
				tagFileWriter.flush();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void closeFile(){
    	if(dataFileWriter != null){
			try {
				dataFileWriter.close();
				dataFileWriter = null;
				tagFileWriter.close();
				tagFileWriter = null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
	
	public void writeData() {
    	String[] weekDays = {"7", "1", "2", "3", "4", "5", "6"};
    	
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DATE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);//小时 
        int minute = cal.get(Calendar.MINUTE);   //分  
        int second = cal.get(Calendar.SECOND);   //秒
        
        String dayOfWeek = weekDays[cal.get(Calendar.DAY_OF_WEEK) - 1];
        
		String line = "" + month + " " + day + " "+ hour + " " + minute + " " + second + " " + dayOfWeek + 
				   " " + acc[0] + " " + acc[1] + " " + acc[2] +
				   " " + gps[0] + " " + gps[1] + " " + gps[2] + 
				   " " + phoneMode + " " + isCharging + " ";
		
		for(int i = 0; i < wifiRes.length; i++){
			line += wifiRes[i].getMacAddress() + " " + wifiRes[i].getLevel() + " ";
		}
		
		line += appRes[0];
		for(int i = 1; i < appRes.length; i++){
			line += " " + appRes[i];
		}
		
		line += "\n";
		
		try {
			dataFileWriter.write(line);
			dataFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public void writeTag(String tag){   
    	
    	Calendar cal = Calendar.getInstance();
    	int month = cal.get(Calendar.MONTH) + 1;
    	int day = cal.get(Calendar.DATE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);//小时 
        int minute = cal.get(Calendar.MINUTE);   //分  
        
        String line = "" + month + " " + day + " " + hour + " " + minute + " " + tag + "\n";
        
        try {
			tagFileWriter.write(line);
			tagFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    public void writeTag(String month,String day,String hour, String minute,String tag){
        String line = "" + month + " " + day + " "+ hour + " " + minute + " " + tag + "\n";
        
        try {
			tagFileWriter.write(line);
			tagFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public double[] getAccelerator(){
    	return acc;
    }
    
    public double[] getLocation(){
    	return gps;
    }
 
    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return mBinder;
    }
 
    @Override
    public void onCreate()
    {
    	sensorService = this;
    	
        startService();
        Log.d(tag, "GPSService Started.");
    }
    
    @SuppressWarnings("deprecation")
	@Override   
    public int onStartCommand(Intent intent, int flags, int startId) {
		Notification notification = new Notification(R.drawable.notify,getString(R.string.app_name), System.currentTimeMillis());  
	    PendingIntent pendingintent = PendingIntent.getActivity(this, 0,  
	    new Intent(this, MainActivity.class), 0);  
	    notification.setLatestEventInfo(this, "正在收集数据...", "",pendingintent);  
        startForeground(0x111, notification);
    	
        flags = START_STICKY;  
        return super.onStartCommand(intent, flags, startId);  
    }
 
    @Override
    public void onDestroy()
    {
        endService();
        stopForeground(true);
        
        Intent intent = new Intent(this,MainActivity.class);
    	startActivity(intent);  
        
        Intent localIntent = new Intent();
        localIntent.setClass(this, SensorService.class); //销毁时重新启动Service
        this.startService(localIntent);
        Log.d(tag, "GPSService Ended.");
        super.onDestroy();
    }
 
    public class GPSServiceBinder extends Binder {
    	SensorService getService() {
            return SensorService.this;
        }
    }

	private class SensorServiceListener implements SensorEventListener{
		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			if( event.sensor.getType() == Sensor.TYPE_ACCELEROMETER ){
				acc[0] = event.values[0];
				acc[1] = event.values[1];
				acc[2] = event.values[2];
			}
		}
	}
	
	private class GPSServiceListener implements LocationListener {
		 
	    @Override
	    public void onLocationChanged(Location location) {
	        // TODO Auto-generated method stub
	        if(location != null)
	        {
	        	gps[0] = (float) location.getLongitude();
				gps[1] = (float) location.getLatitude();
				gps[2] = (float) location.getSpeed();
	        }
	    }
	 
	    @Override
	    public void onProviderDisabled(String provider) {
	        // TODO Auto-generated method stub
	    }
	 
	    @Override
	    public void onProviderEnabled(String provider) {
	        // TODO Auto-generated method stub	 
	    }
	 
	    @Override
	    public void onStatusChanged(String provider, int status, Bundle extras)
	    {
	    }	 
	}
	
	public static class WifiResultReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
				
				List<ScanResult> res = wifiManager.getScanResults();
				if(res != null){
					Collections.sort(res,new Comparator<ScanResult>() {
						@Override
						public int compare(ScanResult lhs, ScanResult rhs) {
							// TODO Auto-generated method stub
							return rhs.level - lhs.level;
						}
					});
					
					WifiInfo wifiInfo = wifiManager.getConnectionInfo();
					wifiRes[0] = new WiFiEntity(wifiInfo.getBSSID(), wifiInfo.getRssi());
					
					int i;
					for(i = 0; i < Math.min(res.size(), 15); i++){
						wifiRes[i + 1] = new WiFiEntity(res.get(i).BSSID, res.get(i).level);
					}	
					
					for(;i < 15; i++){
						wifiRes[i + 1] = new WiFiEntity("NAN",0);
					}
				}else{
					for(int i = 0; i < 16; i++){
						wifiRes[i] = new WiFiEntity("NAN", 0);
					}
				}
			}

		}
	}
	
	public static class WiFiStateReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub 
			scanWifi();
		}

	}

	
	/**
	 * 扫描WIFI
	 */
	private static void scanWifi() {
		if(wifiManager.isWifiEnabled()){
			wifiManager.startScan();
		}else{
			for(int i = 0; i < 16; i++){
				wifiRes[i] = new WiFiEntity("NAN", 0);
			}
		}
	}

	/**
	 * 打开WIFI
	 */
	private void openWifi() {
		for(int i = 0; i < 16; i++){
			wifiRes[i] = new WiFiEntity("NAN", 0);
		}
		if (!wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(true);
		}
		scanWifi();
	}

	/**
	 * 关闭WIFI
	 */
	private void closeWifi() {
		if (wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(false);
		}
	}
	
	/**
	 * 封装定位结果和时间的实体类
	 * 
	 * @author baidu
	 *
	 */
	class LocationEntity {
		BDLocation location;
		long time;
	}
}