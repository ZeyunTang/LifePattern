package com.lifepatterncollector.addtag;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Queue;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.NumberPicker;
import android.widget.TimePicker.OnTimeChangedListener;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.lifepatterncollector.gridview.GridItemAdapter;
import com.lifepatterncollector.main.LifePatternApplication;
import com.lifepatterncollector.main.MainActivity;
import com.lifepatterncollector.main.R;

public class AddTagActivity extends Activity implements OnClickListener{
	private static final int BIGTIMERPICKERWIDTH = 150;
	private static final int BIGTIMERPICKERHEIGHT = 400;
	private static final int SMALLTIMERPICKERWIDTH = 100;
	private static final int SMALLTIMERPICKERHEIGHT = 400;
	private static final int MEDIUMTIMEPICKERWIDTH = 100;
	private static final int MEDIUMTIMEPICKERHEIGHT = 400;
	
	private static final String WORKINGTAG = "Working"; 
	private static final String STUDYTAG = "Study"; 
	private static final String SLEEPSTAG = "Sleep"; 
	private static final String EATINGTAG = "Eating"; 
	private static final String SPORTSTAG = "Sports";
	private static final String SHOPPINGTAG = "Shopping";
	private static final String RELAXTAG = "Relax";
	private static final String HOUSEHOLDTAG = "Household";
	private static final String CINEMATAG = "Cinema";
	private static final String WAKLINGTAG = "Walking";
	private static final String SUBWAYTAG = "Subway";
	private static final String BUSTAG = "Bus";
	private static final String CARTAG = "Car";
	private static final String COMMUNICATIONTAG = "Communication";
	private static final String BLANKTAG = "Blank";
	
	private String[] tags = {WORKINGTAG,STUDYTAG,SLEEPSTAG,EATINGTAG,SPORTSTAG,SHOPPINGTAG,
			RELAXTAG,HOUSEHOLDTAG,CINEMATAG,WAKLINGTAG,SUBWAYTAG,BUSTAG, CARTAG,COMMUNICATIONTAG,BLANKTAG};
	private int[] images = {R.drawable.working,R.drawable.studying,R.drawable.sleeping,R.drawable.eating,R.drawable.sports,R.drawable.shopping
			,R.drawable.relax,R.drawable.household,R.drawable.cinema,R.drawable.walking, R.drawable.subway,R.drawable.bus,R.drawable.car,R.drawable.communication,R.drawable.blank};
	private String[] describtion = {"工作","学习","睡眠","吃饭","运动","购物","娱乐","家务","电影","步行","地铁","公交车","汽车","交流","空"};
	
	private LinearLayout mContainer = null;
	private LayoutInflater mInflater;  
	
	private MyScrollView scrollView;
	
	private GridView gridView;   
	
	private Button completeButton;	
	private LifePatternApplication app;
	
	private List<TagInterval> tagIntervalList;
	private long startTime;
	private long endTime;
	
	private TimePicker startTimePicker;
	private int startTimePickerMonth;
	private int startTimePickerDay;	
	private int startTimePickerHour;
	private int startTimePickerMinutes;
	
	private TimePicker endTimePicker;
	private int endTimePickerMonth;
	private int endTimePickerDay;	
	private int endTimePickerHour;
	private int endTimePickerMinutes;
	
	private int selectedStartTimePickerHour;
	private int selectedStartTimePickerMinutes;
	
	private int selectedEndTimePickerHour;
	private int selectedEndTimePickerMinutes;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addtag);
		
		app = (LifePatternApplication) getApplication();
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, app.beginMonth);
		calendar.set(Calendar.DAY_OF_MONTH, app.beginDay);
		calendar.set(Calendar.HOUR_OF_DAY, app.beginHour);
		calendar.set(Calendar.MINUTE, app.beginMinutes);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		startTime = calendar.getTimeInMillis();		
		startTimePickerMonth = app.beginMonth;
		startTimePickerDay = app.beginDay;		
		startTimePickerHour = app.beginHour;
		startTimePickerMinutes = app.beginMinutes;		
		
		calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, app.endMonth);
		calendar.set(Calendar.DAY_OF_MONTH, app.endDay);
		calendar.set(Calendar.HOUR_OF_DAY, app.endHour);
		calendar.set(Calendar.MINUTE, app.endMinutes);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		endTime = calendar.getTimeInMillis();
		endTimePickerMonth = app.endMonth;
		endTimePickerDay = app.endDay;
		endTimePickerHour = app.endHour;
		endTimePickerMinutes = app.endMinutes;
		
		init();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.completebutton:
				writeTagToFile();
				app.beginMonth = endTimePickerMonth;
	        	app.beginDay = endTimePickerDay;
	        	app.beginHour = endTimePickerHour;
	        	app.beginMinutes = endTimePickerMinutes;
	        	
	        	SharedPreferences beginTime = getSharedPreferences("beginTime", 0);
	            SharedPreferences.Editor editor = beginTime.edit();
	            editor.putInt("beginMonth", app.beginMonth);
	            editor.putInt("beginDay", app.beginDay);
	            editor.putInt("beginHour", app.beginHour);
	            editor.putInt("beginMinutes", app.beginMinutes);
	            editor.commit();
				
				finish();
				return ;
	
			default:
				break;
		}
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_MENU) { //监控/拦截/屏蔽返回键
	        if(completeButton.getVisibility() == View.VISIBLE){
//	        	writeTagToFile();
//	        	app.beginMonth = endTimePickerMonth;
//	        	app.beginDay = endTimePickerDay;
//	        	app.beginHour = endTimePickerHour;
//	        	app.beginMinutes = endTimePickerMinutes;
//	        	
//				finish();
	        	Toast.makeText(this,"当前标记内容被放弃...",Toast.LENGTH_SHORT).show();
	        	finish();
	        }else{
	        	Toast.makeText(this,"当前标记内容被放弃...",Toast.LENGTH_SHORT).show();
	        	finish();
	        }
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onResume() {
//		((MyScrollView)mContainer.getParent()).init();
		super.onResume();
	}
	
	private void writeTagToFile(){
		int i = 0;
		while(i < tagIntervalList.size()){
			tagIntervalList.get(0).writeTag();
			tagIntervalList.remove(0);
			i++;
		}    	
	}
	
	private int getWinWidth(){
		DisplayMetrics dm = new DisplayMetrics();
		//获取屏幕信息
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.widthPixels;
	}
	private int getWinHeight(){
		DisplayMetrics dm = new DisplayMetrics();
		//获取屏幕信息
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.heightPixels;
	}
	
	private void init(){
		tagIntervalList = new ArrayList<TagInterval>();
		
		selectedStartTimePickerHour  = startTimePickerHour;
		selectedStartTimePickerMinutes = startTimePickerMinutes;
		
		startTimePicker = (TimePicker)findViewById(R.id.starttime);        
        startTimePicker.setIs24HourView(true);
        startTimePicker.setCurrentHour(startTimePickerHour);
        startTimePicker.setCurrentMinute(startTimePickerMinutes);
        startTimePicker.setOnTimeChangedListener(new OnTimeChangedListener() {
			
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub	            
//	            selectedStartTimePickerHour = hourOfDay;
//	            selectedStartTimePickerMinutes = minute;
				startTimePicker.setCurrentHour(selectedStartTimePickerHour);
		        startTimePicker.setCurrentMinute(selectedStartTimePickerMinutes);
			}
		});        
        
        selectedEndTimePickerHour = endTimePickerHour;
        selectedEndTimePickerMinutes = endTimePickerMinutes;
        
        endTimePicker = (TimePicker)findViewById(R.id.endtime);
        endTimePicker.setIs24HourView(true);
        endTimePicker.setCurrentHour(endTimePickerHour);
        endTimePicker.setCurrentMinute(endTimePickerMinutes);
        endTimePicker.setOnTimeChangedListener(new OnTimeChangedListener() {
			
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub	            
	            selectedEndTimePickerHour = hourOfDay;
	            selectedEndTimePickerMinutes = minute;
			}
		});
        
        startTimePicker.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
        endTimePicker.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
        
        resizePikcer(startTimePicker);
        resizePikcer(endTimePicker);
		
		scrollView = (MyScrollView) findViewById(R.id.myscrollview);
		
		completeButton = (Button) findViewById(R.id.completebutton);
		completeButton.setVisibility(View.INVISIBLE);
		completeButton.setOnClickListener(this);
		
		mInflater = LayoutInflater.from(this); 		
		mContainer = (LinearLayout) findViewById(R.id.container);
		
		for(int i = 0; i < 10; i++){			
			LayoutParams params = new LayoutParams(getWinWidth(), getWinHeight());
			
			View view = mInflater.inflate(R.layout.tagview, mContainer, false); 
			view.setLayoutParams(params);

			gridView = (GridView) view.findViewById(R.id.gridview);
			gridView.setAdapter(new GridItemAdapter(images, describtion, this));
			
			gridView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub					
					GridItemAdapter adapter = (GridItemAdapter) parent.getAdapter();
		            if(adapter != null && adapter instanceof GridItemAdapter){
	            		Calendar calendar = Calendar.getInstance();
	            		calendar.set(Calendar.MONTH, startTimePickerMonth);
	            		calendar.set(Calendar.DAY_OF_MONTH, startTimePickerDay);
	            		calendar.set(Calendar.HOUR_OF_DAY, selectedStartTimePickerHour);
	            		calendar.set(Calendar.MINUTE, selectedStartTimePickerMinutes);
	            		calendar.set(Calendar.SECOND, 0);
	            		calendar.set(Calendar.MILLISECOND, 0);
	            		
	            		long selectedStartTime = calendar.getTimeInMillis();
	            		if(!((selectedStartTime - startTime) >= 0 && (endTime - selectedStartTime) >= 0)){
	            			Toast.makeText(getBaseContext(),"起始时间不合法",Toast.LENGTH_SHORT).show();
	            			return ;
	            		}
	            		
	            		calendar.set(Calendar.HOUR_OF_DAY, selectedEndTimePickerHour);
	            		calendar.set(Calendar.MINUTE, selectedEndTimePickerMinutes);
	            		long selectedEndTime = calendar.getTimeInMillis();
	            		boolean addDay = false;
	            		
	            		if(selectedEndTime - selectedStartTime < 0){
	            			calendar.add(Calendar.DAY_OF_MONTH, 1);
	            			selectedEndTime = calendar.getTimeInMillis();
	            			addDay = true;
	            		}
	            		
	            		
	            		if(!(selectedEndTime - startTime >= 0 && endTime - selectedEndTime >= 0)){
	            			Toast.makeText(getBaseContext(),"终止时间不合法",Toast.LENGTH_SHORT).show();
	            			return ;
	            		}
	            		TagInterval tagInterval = new TagInterval(startTimePickerMonth,startTimePickerDay, selectedStartTimePickerHour,
	            				selectedStartTimePickerMinutes, selectedEndTimePickerHour, selectedEndTimePickerMinutes);
	            		tagInterval.setTag(tags[position]);
	            		
	            		tagIntervalList.add(tagInterval);
	            		
	            		if(Math.abs(selectedEndTime - endTime) / 60000 < 1){
	            			completeButton.setVisibility(View.VISIBLE);
	            		}else{
	            			if(addDay){
		            			startTimePickerMonth = calendar.get(Calendar.MONTH);
		            			startTimePickerDay = calendar.get(Calendar.DAY_OF_MONTH);			            			
		            		}
	            			
	            			
	            			selectedStartTimePickerHour = selectedEndTimePickerHour;
	            			selectedStartTimePickerMinutes = selectedEndTimePickerMinutes;
	            			startTimePicker.setCurrentHour(selectedEndTimePickerHour);
	            			startTimePicker.setCurrentMinute(selectedEndTimePickerMinutes);	            			
	            			
	            			endTimePicker.setCurrentHour(endTimePickerHour);
	            			endTimePicker.setCurrentMinute(endTimePickerMinutes);
	            			scrollView.nextPage();
	            		}
		            	
		                adapter.setFocusPosition(position);
		                adapter.notifyDataSetChanged();
		            }
				}
			});
			
			mContainer.addView(view);
		}
	}
	
	private void resizePikcer(FrameLayout tp){
		List<NumberPicker> npList = findNumberPicker(tp);
		for(NumberPicker np:npList){
			resizeNumberPicker(np);
		}
	}
	
	/**
	 * 得到viewGroup里面的numberpicker组件
	 * @param viewGroup
	 * @return
	 */
	private List<NumberPicker> findNumberPicker(ViewGroup viewGroup){
		List<NumberPicker> npList = new ArrayList<NumberPicker>();
		View child = null;
		if(null != viewGroup){
			for(int i = 0;i<viewGroup.getChildCount();i++){
				child = viewGroup.getChildAt(i);
				if(child instanceof NumberPicker){
					npList.add((NumberPicker)child);
				}
				else if(child instanceof LinearLayout){
					List<NumberPicker> result = findNumberPicker((ViewGroup)child);
					if(result.size()>0){
						return result;
					}
				}
			}
		}
		return npList;
	}
	
	/*
	 * 调整numberpicker大小
	 */
	private void resizeNumberPicker(NumberPicker np){
		WindowManager wm = this.getWindowManager();		 
	    int width = wm.getDefaultDisplay().getWidth();
	    int height = wm.getDefaultDisplay().getHeight();
	    if(width >= 1000){
	    	width = BIGTIMERPICKERWIDTH;
	    	height = BIGTIMERPICKERHEIGHT;
	    }else if(width >= 500 && width < 1000){
	    	width = MEDIUMTIMEPICKERWIDTH;
	    	height = MEDIUMTIMEPICKERHEIGHT;
	    }else{
	    	width = SMALLTIMERPICKERWIDTH;
	    	height = SMALLTIMERPICKERHEIGHT;
	    }
		
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
		params.setMargins(7, 0, 7, 0);
		np.setLayoutParams(params);
	}
}
