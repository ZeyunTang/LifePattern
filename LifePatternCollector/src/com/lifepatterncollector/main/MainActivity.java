package com.lifepatterncollector.main;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.lifepatterncollector.addtag.AddTagActivity;
import com.lifepatterncollector.addtag.TagInterval;
import com.lifepatterncollector.alarm.AndroidAlarm;
import com.lifepatterncollector.sensor.SensorService;
import com.lifepatterncollector.sensor.WiFiEntity;

public class MainActivity extends Activity implements OnClickListener{
	public static  MainActivity mainActivity = null;
	
	private LifePatternApplication app;
	
	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mBuilder;
	private int notifyId = 0;
	
	private Window mWindow;
	
	private TextView gpsDataTextView;
	private TextView accDataTextView;	
	
	private Button controlButton;
	private Button addTagButton;
	private boolean nextStateIsStart;
	
	private TimePicker timePicker;
	
	private AndroidAlarm notificationAlarm;
	
//	private AndroidAlarm addTagIntervalAlarm;
	
	private AndroidAlarm weakupAlarm;
	private int weakupAlarmHour;
	private int weakupAlarmMinute;
	
	
//	private WakeLock wakeLock = null;
	
	//Sensor Data
	public double[] gps;
	public double[] acc;
	public WiFiEntity[] wifiRes;
	public int phoneMode;
	public int isCharging;
	public String[] appRes;
	
	public TextView stateTextView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_main);	
		
//		acquireWakeLock();
		
		mainActivity = this;
		app = (LifePatternApplication)getApplication();
		
		weakupAlarmHour = app.alarmHour;
		weakupAlarmMinute = app.alarmMinute;
		
		init();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		stopRecord();
		if(weakupAlarm != null){
			weakupAlarm.stopAlarm();
			weakupAlarm = null;
		}
		
	};
	
    @Override  
    public boolean onKeyDown(int keyCode, KeyEvent event) {  
        if (keyCode == KeyEvent.KEYCODE_BACK) {  
            moveTaskToBack(false);  
            return true;  
        }  
        return super.onKeyDown(keyCode, event);  
    }  
	
	private void init(){		
		//初始化界面
		gpsDataTextView = (TextView) findViewById(R.id.tv_gps);
		accDataTextView = (TextView) findViewById(R.id.tv_acc);
		stateTextView = (TextView) findViewById(R.id.tv_state);

		controlButton = (Button) findViewById(R.id.controlbutton);
		addTagButton = (Button) findViewById(R.id.addtagbutton);
		
		controlButton.setOnClickListener(this);
		addTagButton.setOnClickListener(this);
        
        startRecord();
	}
	
	public void updateLocation() {
		// TODO Auto-generated method stub
		if (gps != null) {
			StringBuilder str = new StringBuilder();
			str.append("实时位置信息:\n");
			str.append("经度:");
			str.append(gps[0]);
			str.append("\n纬度:");
			str.append(gps[1]);
			str.append("\n速度:");
			str.append(gps[2]);
			gpsDataTextView.setText(str);
		} else {
			gpsDataTextView.setText("There is no available gps data for now");
		}
	}
	
	public void updateAccelerator(){
		StringBuilder str = new StringBuilder();
		str.append("加速度:\n");
		str.append("X:");
		str.append(acc[0]);
		str.append("\nY:");
		str.append(acc[1]);
		str.append("\nZ:");
		str.append(acc[2]);
		accDataTextView.setText(str);
	}    
    
    public void startRecord(){
    	nextStateIsStart = false;
    	controlButton.setText("点击此处停止记录");
    	addTagButton.setEnabled(true);
    	
    	SharedPreferences beginTime = getSharedPreferences("beginTime", 0);
        SharedPreferences.Editor editor = beginTime.edit();
        int beginMonth = beginTime.getInt("beginMonth", -1);
        
        if(beginMonth == -1){
        	Calendar cal = Calendar.getInstance();
        	int month = cal.get(Calendar.MONTH) + 1;
        	int day = cal.get(Calendar.DAY_OF_MONTH);
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE); 
        
            app.beginMonth = month;
            app.beginDay = day;        
            app.beginHour = hour;
            app.beginMinutes = minute;            
        }else{
        	app.beginMonth = beginTime.getInt("beginMonth", -1);
        	app.beginDay = beginTime.getInt("beginDay", -1);
        	app.beginHour = beginTime.getInt("beginHour", -1);
        	app.beginMinutes = beginTime.getInt("beginMinutes", -1);
        	
        	Calendar cal = Calendar.getInstance();
        	cal.set(Calendar.SECOND, 0);
        	cal.set(Calendar.MILLISECOND, 0);
        	long nowtime = cal.getTimeInMillis();
        	
        	cal.set(Calendar.MONTH, app.beginMonth);
        	cal.set(Calendar.DAY_OF_MONTH, app.beginDay);
        	cal.set(Calendar.HOUR_OF_DAY, app.beginHour);
        	cal.set(Calendar.MINUTE, app.beginMinutes);
        	
        	long oldTime = cal.getTimeInMillis();
        	
        	if((nowtime - oldTime) / (24 * 60 * 60 * 1000) >= 1){
        		cal = Calendar.getInstance();
            	int month = cal.get(Calendar.MONTH) + 1;
            	int day = cal.get(Calendar.DAY_OF_MONTH);
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE); 
            
                app.beginMonth = month;
                app.beginDay = day;        
                app.beginHour = hour;
                app.beginMinutes = minute;
        	}
        }
        
        editor = beginTime.edit();
        editor.putInt("beginMonth", app.beginMonth);
        editor.putInt("beginDay", app.beginDay);
        editor.putInt("beginHour", app.beginHour);
        editor.putInt("beginMinutes", app.beginMinutes);
        editor.commit();
    	
    	
        
//        notificationAlarm = new Alarm(hour, minute + 1, app.notificationIntervalMinute , MainActivity.this,Alarm.NOTIFICATION);
       
//        addTagIntervalAlarm = new AndroidAlarm(hour, minute + app.tagIntervalMinutes, app.tagIntervalMinutes,MainActivity.this,AndroidAlarm.ADDTAG);
        
        notificationAlarm = new AndroidAlarm(Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1, 0, app.notificationIntervalMinute,MainActivity.this,AndroidAlarm.NOTIFICATION);
    	if(weakupAlarm != null){
    		weakupAlarm.stopAlarm();
    		weakupAlarm = null;
    	}
    	
    	
    	stateTextView.setText("当前状态：正在记录");
		startService(new Intent(MainActivity.this, SensorService.class));
		
//		Toast.makeText(getBaseContext(),"开始记录",Toast.LENGTH_SHORT).show();
    }
    
    private void stopRecord(){
    	nextStateIsStart = true;
    	controlButton.setText("点击此处开始记录");
    	addTagButton.setEnabled(false);
    	
    	stateTextView.setText("当前状态：停止记录");
		
		notificationAlarm.stopAlarm();
		notificationAlarm = null;
		
	    
//		stopService(new Intent(MainActivity.this, SensorService.class));
    }
    
    public void addTagInterval(){
    	Calendar cal = Calendar.getInstance();
    	int month = cal.get(Calendar.MONTH) + 1; //月份
    	int day = cal.get(Calendar.DAY_OF_MONTH); //日
        int hour = cal.get(Calendar.HOUR_OF_DAY);//小时 
        int minute = cal.get(Calendar.MINUTE);   //分 
        if(minute % 15 >= 12){
        	minute = ((minute + 14) / 15) * 15;
        }else{
        	minute = (minute / 15) * 15;
        }
        
        if(minute == 60){
    		hour++;
    		if(hour == 24){
    			hour = 0;
    		}
    		minute = 0;
    	}
        
        if(minute != app.beginMinutes){
        	app.unLabledInterval.add(new TagInterval(month,day,app.beginHour,app.beginMinutes,hour,minute));
        }
        app.beginHour = hour;
        app.beginMinutes = minute;
    }
    
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
			case R.id.controlbutton:
				if(nextStateIsStart){					
					startRecord();
				}else{
//					showPopupWindow(MainActivity.this, addTagButton);
					showAlarmDialog();
				}
				break;
			case R.id.addtagbutton:
				Calendar cal = Calendar.getInstance();
		    	int month = cal.get(Calendar.MONTH) + 1; //月份
		    	int day = cal.get(Calendar.DAY_OF_MONTH); //日
		        int hour = cal.get(Calendar.HOUR_OF_DAY);//小时 
		        int minute = cal.get(Calendar.MINUTE);   //分 
		        
		        app.endMonth = month;
		        app.endDay = day;
		        app.endHour = hour;
		        app.endMinutes = minute;
				
				Intent intent = new Intent(this,AddTagActivity.class);
		    	startActivity(intent);   
				break;
		}		
	}
	
	/** 显示通知栏点击跳转到指定Activity */
	public void showIntentActivityNotify(){
		//初始化通知栏
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);		
		mBuilder = new NotificationCompat.Builder(this);
		mBuilder.setContentTitle("该标注活动啦~")
				.setContentText("该标注活动啦~")
				.setTicker("该标注活动啦~")//通知首次出现在通知栏，带上升动画效果的
				.setPriority(Notification.PRIORITY_DEFAULT)//设置该通知优先级  
				.setDefaults(Notification.DEFAULT_ALL)//向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：
				.setSmallIcon(R.drawable.notify)
				.setAutoCancel(true)
		        .setWhen(System.currentTimeMillis());
		
        //点击的意图ACTION是跳转到Intent
//		Intent resultIntent = new Intent(this, AddTagActivity.class);
//		resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//		mBuilder.setContentIntent(pendingIntent);
		Notification notification = mBuilder.build();
//		notification.flags |= Notification.FLAG_NO_CLEAR;
		mNotificationManager.notify(notifyId++, notification);
	}
	
	private void showAlarmDialog() {		
        LayoutInflater inflater = LayoutInflater.from(this);  
        final View alarmView = inflater.inflate(  
                R.layout.alarmdialog, null);  
        
        timePicker =(TimePicker)alarmView.findViewById(R.id.tp);
        timePicker.setIs24HourView(true);//是否显示24小时制？默认false
        timePicker.setCurrentHour(app.alarmHour);
        timePicker.setCurrentMinute(app.alarmMinute);
        timePicker.setOnTimeChangedListener(new OnTimeChangedListener() {
			
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
	            
	            weakupAlarmHour = hourOfDay;
	            weakupAlarmMinute = minute;
			}
		});
        
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);  
        builder.setCancelable(false);  
        builder.setIcon(R.drawable.ic_launcher);  
        builder.setTitle("请设置恢复记录时刻");  
        builder.setView(alarmView); 
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
        builder.setPositiveButton("确认",  
                new DialogInterface.OnClickListener() {  
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if(weakupAlarm != null){
                        	weakupAlarm.stopAlarm();
                        	weakupAlarm = null;
                        }

                    	app.alarmHour = weakupAlarmHour;
                    	app.alarmMinute = weakupAlarmMinute;
                    	
                    	weakupAlarm = new AndroidAlarm(weakupAlarmHour, weakupAlarmMinute,MainActivity.this,AndroidAlarm.WEAKUP);
                    	stopRecord();
                    }  
                });
        builder.show();  
    } 
	
	//获取电源锁，保持该服务在屏幕熄灭时仍然获取CPU时，保持运行  
//    private void acquireWakeLock()  
//    {  
//        if (null == wakeLock)  
//        {  
//            PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);  
//            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK|PowerManager.ON_AFTER_RELEASE, "PostLocationService");  
//            if (null != wakeLock)  
//            {  
//                wakeLock.acquire();  
//            }  
//        }  
//    }  
//      
//    //释放设备电源锁  
//    private void releaseWakeLock()  
//    {  
//        if (null != wakeLock)  
//        {  
//            wakeLock.release();  
//            wakeLock = null;  
//        }  
//    }  
	
//	public void showPopupWindow(Context context,View parent){		
//        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);     
//        final View vPopupWindow=inflater.inflate(R.layout.popupwindow, null, false);  
//        final PopupWindow pw= new PopupWindow(vPopupWindow,1000,1000,true);  
//                
//		timePicker =(TimePicker)vPopupWindow.findViewById(R.id.tp);
//		timePicker.setIs24HourView(true);//是否显示24小时制？默认false
//		timePicker.setCurrentHour(app.alarmHour);
//		timePicker.setCurrentMinute(app.alarmMinute);
//		timePicker.setOnTimeChangedListener(new OnTimeChangedListener() {
//				
//				@Override
//				public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
//					// TODO Auto-generated method stub
//		        
//		        weakupAlarmHour = hourOfDay;
//		        weakupAlarmMinute = minute;
//			}
//		});       
//  
//        //OK按钮及其处理事件  
//        Button btnOK=(Button)vPopupWindow.findViewById(R.id.popwindowsokbutton);  
//        btnOK.setOnClickListener(new OnClickListener(){  
//            @Override  
//            public void onClick(View v) {  
//                //设置文本框内容  
//            	if(weakupAlarm != null){
//                	weakupAlarm.stopAlarm();
//                	weakupAlarm = null;
//                }
//
//            	app.alarmHour = weakupAlarmHour;
//            	app.alarmMinute = weakupAlarmMinute;
//            	
//            	weakupAlarm = new Alarm(weakupAlarmHour, weakupAlarmMinute, MainActivity.this, Alarm.WEAKUP);
//            	
//            	stopRecord();
//                pw.dismiss();
//            }  
//        });  
//          
//      //Cancel按钮及其处理事件  
//        Button btnCancel=(Button)vPopupWindow.findViewById(R.id.popwindowcancelbutton);  
//        btnCancel.setOnClickListener(new OnClickListener(){  
//            @Override  
//            public void onClick(View v) {
//                pw.dismiss();//关闭  
//            }  
//        });  
//        
//        ColorDrawable dw = new ColorDrawable(0);  
//        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作  
//        pw.setBackgroundDrawable(dw);  
//        
//        WindowManager.LayoutParams params=mainActivity.getWindow().getAttributes();  
//        params.alpha=0.7f;  
//            
//        mainActivity.getWindow().setAttributes(params); 
//        
//        pw.setOutsideTouchable(true);    
//        
//        int popupWidth = vPopupWindow.getMeasuredWidth();        
//        pw.showAtLocation(parent, Gravity.CENTER, - popupWidth / 2,0); 
//    }  

}