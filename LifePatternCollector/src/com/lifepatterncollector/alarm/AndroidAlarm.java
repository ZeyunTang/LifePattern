package com.lifepatterncollector.alarm;

import java.util.Calendar;
import java.util.TimeZone;

import com.lifepatterncollector.main.MainActivity;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

public class AndroidAlarm {
	public static String NOTIFICATION = "NotificationAlarm";
	public static String WEAKUP = "WeakupAlarm";
	public static String ADDTAG = "AddTag";
	
	private String TAG = "Alarm";
	
	private PendingIntent alarmSender;
	
	private String action;
	private int hour;
	private int minute;
	private long deltaMillisecond;
	private Context context;
	private boolean isOn;
	
	public AndroidAlarm(int hour,int minute,int deltaMinute,Context context,String alarmtype){
		this.hour = hour;
		this.minute = minute;
		this.deltaMillisecond = deltaMinute * 60 * 1000;
		this.context = context;
		this.action = alarmtype;
		
		initAlarm();
	}
	
	public AndroidAlarm(int hour,int minute,Context context,String alarmtype){
		this.hour = hour;
		this.minute = minute;
		this.deltaMillisecond = 24 * 60 * 60000;
		this.context = context;
		this.action = alarmtype;
		
		initAlarm();
	}
	
	public void stopAlarm(){
		if(!isOn){
			return ;
		}
		
		isOn = false;
		
		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(alarmSender);
	}
	
	private void initAlarm(){
		isOn = true;
		
		Intent intent = new Intent(context, AndroidAlarmReceiver.class);
		intent.setAction(action);
		
		int requestCode = 0;
		if(action.equals(WEAKUP)){
			requestCode = 0;
		}else if(action.equals(ADDTAG)){
			requestCode = 1;
		}else if(action.equals(NOTIFICATION)){
			requestCode = 2;
		}
		
		alarmSender = PendingIntent.getBroadcast(context, requestCode, intent,0);

		long firstTime = SystemClock.elapsedRealtime();			
		long systemTime = System.currentTimeMillis();

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		long selectTime = calendar.getTimeInMillis();
		if(systemTime > selectTime) {
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			selectTime = calendar.getTimeInMillis();
		}
		
		long time = selectTime - systemTime;
		firstTime += time;
		
		AlarmManager manager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		manager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,firstTime, deltaMillisecond, alarmSender);
	}
}
