package com.lifepatterncollector.addtag;

import com.lifepatterncollector.main.MainActivity;
import com.lifepatterncollector.sensor.SensorService;

public class TagInterval{
	private String beginMonth;
	private String beginDay;
	
	private String beginHour;
	private String beginMinutes;
	
	private String endHour;
	private String endMinutes;
	
	private boolean isTagged;
	private String tag = "";
	
	public String getBeginHour() {
		return beginHour;
	}

	public void setBeginHour(String beginHour) {
		this.beginHour = beginHour;
	}

	public String getBeginMinutes() {
		return beginMinutes;
	}

	public void setBeginMinutes(String beginMinutes) {
		this.beginMinutes = beginMinutes;
	}

	public String getEndHour() {
		return endHour;
	}

	public void setEndHour(String endHour) {
		this.endHour = endHour;
	}

	public String getBeginMonth() {
		return beginMonth;
	}

	public String getBeginDay() {
		return beginDay;
	}

	public String getEndMinutes() {
		return endMinutes;
	}

	public void setEndMinutes(String endMinutes) {
		this.endMinutes = endMinutes;
	}

	
	public void setBeginMonth(String beginMonth) {
		this.beginMonth = beginMonth;
	}

	public void setBeginDay(String beginDay) {
		this.beginDay = beginDay;
	}

	public TagInterval(int month,int day,int beginHour,int beginMinutes,int endHour,int endMinutes){
		this.beginMonth = "" + month;
		this.beginDay = "" + day;
		this.beginHour = "" + beginHour;
		this.beginMinutes = "" + beginMinutes;
		this.endHour = "" + endHour;
		this.endMinutes = "" + endMinutes;
	}
	
	public boolean isTagged(){
		return isTagged;
	}
	
	public void setTag(String tag){
		isTagged = true;
		this.tag = tag;
	}
	
	public void writeTag(){
		SensorService.sensorService.writeTag(beginMonth,beginDay,beginHour, beginMinutes, tag);
	}
}