package com.lifepatterncollector.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.lifepatterncollector.main.MainActivity;

public class AndroidAlarmReceiver extends BroadcastReceiver {
	@Override
    public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		
		if(action.equals(AndroidAlarm.NOTIFICATION)){
		    MainActivity.mainActivity.showIntentActivityNotify();
		}else if(action.equals(AndroidAlarm.WEAKUP)){
			MainActivity.mainActivity.startRecord();
		}else{
			MainActivity.mainActivity.addTagInterval();
		}
    }
}