package com.lifepatterncollector.unused;

import com.lifepatterncollector.main.LifePatternApplication;
import com.lifepatterncollector.sensor.SensorService;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class TimerService extends Service{
	public static String NOTIFICATION = "NotificationAlarm";
	public static String WEAKUP = "WeakupAlarm";
	public static String ADDTAG = "AddTag";
	
	private TimerServiceBinder mBinder = new TimerServiceBinder();
	private LifePatternApplication app;
	
	private int notificationAlarmInterval;
	private int addTagInterval;
	
	 @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return mBinder;
    }
 
    @Override
    public void onCreate()
    {
    	app = (LifePatternApplication) getApplication();
    	notificationAlarmInterval = app.notificationIntervalMinute;
    	addTagInterval = app.tagIntervalMinutes;
    	
        startService();
    }
 
    @Override
    public void onDestroy()
    {
        endService();
    }
    
    public class TimerServiceBinder extends Binder {
    	TimerService getService() {
            return TimerService.this;
        }
    }
    
    private void startService(){
    	
    }
    
    private void endService(){
    	
    }
}
